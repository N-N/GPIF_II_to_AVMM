//Avalon streaming sink to memory mapped master converter
//N_N

module avst_to_avmm_master #( parameter 	AVST_DATA_WIDTH=32, 
								AVMM_ADDR_WIDTH = 32, 
								AVMM_DATA_WIDTH=512,
								DATA_CNT_WIDTH = 36,
								AVMM_BURST_WIDTH=4, 
								AVMM_BURSTCOUNT = 8,
								FIFO_DEPTH = 64	
								)
(

	input wire 			avst_clk,
	input wire 			avst_rstn,
	
	input wire 			avmm_clk,
	input wire 			avmm_rstn,
	
	//AVST sink
	input wire 							avst_valid,
	input wire [AVST_DATA_WIDTH-1:0] 	avst_data,
	output reg 							avst_ready,
	
	//AVMM master
	input wire 							avmm_waitrequest,
	output reg 							avmm_write,
	output wire [AVMM_DATA_WIDTH-1:0] 	avmm_writedata,
	output reg [AVMM_ADDR_WIDTH-1:0] 	avmm_address,	
	output wire [AVMM_BURST_WIDTH-1:0]	avmm_burstcount,
	
	//Control signals
	input wire 							start, // pulse
	input wire [AVMM_ADDR_WIDTH-1:0]	base_address,
	input wire [31:0]					lenght,
	output reg 							busy

);

assign	avmm_burstcount = AVMM_BURSTCOUNT;

//AVST state machine
parameter IDLE = 4'd0, WAIT_AVMM_READY = 4'b1, AVST_WRITE = 4'd2, WAIT_AVMM_DONE = 4'd3;
reg [3:0]state, n_state;

//AVMM state machine
parameter AVMM_READY = 4'd1, AVMM_WRITE = 4'd2;
reg [3:0] avmm_state, n_avmm_state;

//AVMM
reg n_avmm_write;
reg [AVMM_ADDR_WIDTH-1:0] n_avmm_address;

//Control
reg n_busy;
wire start_avmm_clk, avst_ready_avmm_clk;

wire avmm_ready_avst_clk; 
reg avmm_ready, n_avmm_ready;

reg avmm_done, n_avmm_done;
wire avmm_done_avst_clk;

reg avst_done, n_avst_done;
wire avst_done_avmm_clk;

//Data counter
reg [DATA_CNT_WIDTH-1:0] avst_data_cnt, n_avst_data_cnt;

//FIFO
wire fifo_wr_en, fifo_rd, fifo_wrfull, fifo_rdempty, fifo_aclr;
reg fifo_wr, fifo_clr, n_fifo_clr, fifo_rd_en , n_fifo_rd_en;
assign fifo_aclr = !avst_rstn | fifo_clr;

always @(*)
begin
	n_state = state;
	n_avst_data_cnt = avst_data_cnt;
	n_busy = busy;
	n_fifo_clr = 1'b0;
	avst_ready = 1'b0;
	n_avst_done = avst_done;
	
	fifo_wr=1'b0;
	
	case (state)
		IDLE:
		begin
			if (start)
			begin
				n_state = WAIT_AVMM_READY;
				n_busy = 1'b1;
				n_fifo_clr = 1'b1;
				n_avst_done = 1'b0;
			end 
			else
			begin
				n_busy = 1'b0;
				n_avst_data_cnt = 0;
			end
		end
		
		WAIT_AVMM_READY:
		begin
			if(avmm_ready_avst_clk)
				n_state = AVMM_WRITE;
		end
		
		AVMM_WRITE:
		begin
			if (avst_data_cnt[DATA_CNT_WIDTH-1:$clog2(AVMM_DATA_WIDTH/AVST_DATA_WIDTH)] >= lenght)
			begin
				fifo_wr=1'b0;
				n_avst_done = 1'b1;
				n_state = WAIT_AVMM_DONE;
			end
			else if (!fifo_wrfull)
			begin
				avst_ready = 1'b1;
				if (avst_valid)
				begin
					fifo_wr=1'b1;
					n_avst_data_cnt = avst_data_cnt + 1;
				end
			end	
			
		end
		
		WAIT_AVMM_DONE:
		begin
			if(avmm_done_avst_clk)
				n_state = IDLE;
		end
	endcase
end

always @(posedge avst_clk or negedge avst_rstn)
begin
	if (!avst_rstn)
	begin
		state <= IDLE;
		avst_data_cnt <= 0;
		busy <= 1'b0;
		fifo_clr <= 1'b0;
		avst_done <= 1'b0;
	end
	else
	begin
		state <= n_state;
		avst_data_cnt <= n_avst_data_cnt;
		busy <= n_busy;
		fifo_clr <= n_fifo_clr;
		avst_done <= n_avst_done;
	end
end

data_sync sync0(
	.data		(start),
	.dest_clk	(avmm_clk),
	.dest_rstn	(avmm_rstn),
	.data_sync	(start_avmm_clk)
);

data_sync sync1(
	.data		(avst_ready),
	.dest_clk	(avmm_clk),
	.dest_rstn	(avmm_rstn),
	.data_sync	(avst_ready_avmm_clk)
);

data_sync sync2(
	.data		(avmm_ready),
	.dest_clk	(avst_clk),
	.dest_rstn	(avst_rstn),
	.data_sync	(avmm_ready_avst_clk)
);

data_sync sync3(
	.data		(avst_done),
	.dest_clk	(avmm_clk),
	.dest_rstn	(avmm_rstn),
	.data_sync	(avst_done_avmm_clk)
);

data_sync sync4(
	.data		(avmm_done),
	.dest_clk	(avst_clk),
	.dest_rstn	(avst_rstn),
	.data_sync	(avmm_done_avst_clk)
);

//FIFO
dcfifo_mixed_widths	wr_fifo (
				.aclr 		(fifo_aclr),
				.data 		(avst_data),
				.rdclk 		(avmm_clk),
				.rdreq 		(fifo_rd),
				.wrclk 		(avst_clk),
				.wrreq 		(fifo_wr),
				.q 			(avmm_writedata),
				.rdempty 	(fifo_rdempty),
				.wrusedw 	(),
				.eccstatus 	(),
				.rdfull 	(),
				.rdusedw 	(),
				.wrempty 	(),
				.wrfull 	(fifo_wrfull));
	defparam
		wr_fifo.intended_device_family = "Arria V GZ",
		wr_fifo.lpm_numwords = FIFO_DEPTH,
		wr_fifo.lpm_showahead = "OFF",
		wr_fifo.lpm_type = "dcfifo_mixed_widths",
		wr_fifo.lpm_width = AVST_DATA_WIDTH,
		wr_fifo.lpm_widthu = $clog2(FIFO_DEPTH),
		wr_fifo.lpm_widthu_r = $clog2(FIFO_DEPTH*AVST_DATA_WIDTH/AVMM_DATA_WIDTH),
		wr_fifo.lpm_width_r = AVMM_DATA_WIDTH,
		wr_fifo.overflow_checking = "ON",
		wr_fifo.rdsync_delaypipe = 5,
		wr_fifo.read_aclr_synch = "OFF",
		wr_fifo.underflow_checking = "ON",
		wr_fifo.use_eab = "ON",
		wr_fifo.write_aclr_synch = "OFF",
		wr_fifo.wrsync_delaypipe = 5;
		
assign fifo_rd = ((!avmm_waitrequest) || fifo_rd_en) && !fifo_rdempty;
	
always @ (*)
begin
	n_avmm_write 	= avmm_write;
	n_avmm_address 	= avmm_address;
	n_fifo_rd_en 	= fifo_rd_en;
	n_avmm_ready 	= avmm_ready;
	n_avmm_done 	= avmm_done;
	n_avmm_state 	= avmm_state;
	
	case (avmm_state)
		IDLE:
		begin
			if (start_avmm_clk)
			begin
				n_avmm_address = base_address;
				n_avmm_ready = 1'b1;
				n_avmm_done = 1'b0;
				n_avmm_state = AVMM_READY;
			end	
		end
		
		AVMM_READY:
		begin
			if(avst_ready_avmm_clk)
			begin
				n_avmm_ready = 1'b0;
				n_avmm_state = AVMM_WRITE;
			end	
		end
		
		AVMM_WRITE:
		begin
			//Start mem writing when any data appears in the fifo
			if( !fifo_rdempty )
				n_avmm_write = 1'b1;
			else
			begin
				n_fifo_rd_en = 1'b0;
				//Even if fifo is empty it has last data on its output
				//Hold write until last word will be writen
				if(!avmm_waitrequest)
					n_avmm_write = 1'b0;
			end	
			//Read one word from fifo and wait if waitrequest
			//Or just keep reading fifo
			if(avmm_write && avmm_waitrequest)
				n_fifo_rd_en = 1'b0;
			else if (!fifo_rdempty)
				n_fifo_rd_en = 1'b1;
			
			//Stop condition
			if( avst_done_avmm_clk && fifo_rdempty)
			begin
				n_avmm_done = 1'b1;
				n_avmm_state = IDLE;
			end	
			
			//Address counter	
			else if(avmm_write && !avmm_waitrequest)
				n_avmm_address = avmm_address + 32'd1;	
		end
	endcase		
end

always @(posedge avmm_clk or negedge avmm_rstn)
begin
	if(!avmm_rstn)
	begin
		avmm_state		<= IDLE;
		avmm_address	<= 32'b0;				
		avmm_write		<= 1'b0;
		fifo_rd_en		<= 1'b0;
		avmm_done		<= 1'b0;
		avmm_ready		<= 1'b0;
	end
	else if (avmm_clk)
	begin
		avmm_state	<= n_avmm_state;
		avmm_address<= n_avmm_address;
		avmm_write 	<= n_avmm_write;
		avmm_ready 	<= n_avmm_ready;
		avmm_done	<= n_avmm_done;
		fifo_rd_en 	<= n_fifo_rd_en;
	end
end	
		
endmodule
