//////////////////////////////////////////////////////////////////////////////////
// Company: 		Soliton Technologies
// Engineer: 		Prakash/Gokul
// 
// Create Date:   23/08/2014 
// Design Name: 	USB3 FX3
// Module Name:   fx3_main 
// Project Name: 	TSW14J56 _USB3 project
// Target Devices: Arria V
// Tool versions:  Quartus 14
// Description: 
//
// Dependencies: 
//
// Revision: 		0.0
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

//Author : Ebenezer Dwobeng  ebenezer-d@ti.com



module fx3toddr #(parameter M=4,SAMPLES_PER_CLK =4,WR_ADDR_WIDTH=32,PRBS_L=16, PRBS_M=9,PRBS_N=5
)
(

	input  wire                  clk_fx3, //clock and reset associated with the slave interface
	input  wire                  clk_ddr,
	input  wire                  reset_n,
	//input  wire                  afi_clock,//clock and reset associated with the master interface
	//input  wire                  afi_rstn,
	
	input  wire                dac_wrfifo_valid,
	input  wire [31:0]       	dac_wrfifo_data,
	output wire                dac_wrfifo_ready,
	
	// Avalon Memory Mapped Slave Master: used to write to external DDR3A memory
	output wire						 mem_avm_m1_read,
	input wire						 mem_avm_m1_waitrequest,
	input wire	[511:0]			 mem_avm_m1_readdata,
	output wire						 mem_avm_m1_write,
	output wire	[511:0]			 mem_avm_m1_writedata,
	input wire						 mem_avm_m1_readdatavalid,
	output wire	[31:0]			 mem_avm_m1_address,	
	output wire	[3:0]			 	 mem_avm_m1_burstcount,

	input wire 						dac_start,
	input wire [31:0]				dac_size,
	input wire 						dac_xfr_enable,
	input wire 						xcvr_mode

	
	);
	
	//import jesdcon_pkg::*; //get the log2 function

	parameter 		BURSTCOUNT = 8;
	parameter 		IDLE = 2'b00;	      
	parameter 		AVST_WRITE = 2'b01;
				
	parameter 		RD_IDLE = 1'b0;		
	parameter 		SEND_DATA = 1'b1;


	reg [31:0]  	ddr3a_mem_wr_addr,n_ddr3a_mem_wr_addr;//,ddr3b_mem_wr_addr,n_ddr3b_mem_wr_addr;
	reg [31:0]		ddr3a_mem_wr_addr_r;
	reg [31:0]  	mem_wr_data;
	reg            mem_wr_en, n_mem_wr_en, ddr3a_wr_en,n_ddr3a_wr_en;//,ddr3b_wr_en,n_ddr3b_wr_en;
	reg [3:0]		mem_wr_en_r;	
	reg [1:0] 		state, n_state;
	reg      		rd_start_a;//, rd_start_b;
	reg [511:0]		mem_avm_m1_writedata_r;
	
	wire 				empty_a;//, empty_b;
	wire [7:0]		wrusedw_a;//, wrusedw_b;
	wire [511:0]	mem_avm_m1_writedata_fifo;
	
//----------------------------------  FIFO section ----------------------------
reg	[7:0] 	wrusedw_a_r;
reg [31:0]		fifo_in;
reg				fifo_wren;

reg afull_fifo;

// register FIFO signals on fx3 clk
always @(posedge clk_fx3 or negedge reset_n)
begin
	if(!reset_n)
	begin
		fifo_in 		<= 32'd0;
		fifo_wren 		<= 1'b0;
		wrusedw_a_r		<= 8'h0;
	end	
	else
	begin
		fifo_in 		<= mem_wr_data;
		fifo_wren 		<= mem_wr_en;
		wrusedw_a_r		<=	wrusedw_a;
	end
end

// Fifo0 ... DDR read FX3 writes	
	fx32ddr_32bfifo	fifo0 
	(
		.data				(fifo_in),// register separately before writing
		.wrclk			(clk_fx3),
		.wrusedw			(wrusedw_a),
		.wrreq 			(fifo_wren),
		
		.rdclk			(clk_ddr),
		.rdreq 			(!mem_avm_m1_waitrequest),
		.rdempty			(empty_a),
		.q 				(mem_avm_m1_writedata_fifo),	//This is sent without registering modify; prakash
		
		.aclr				(dac_start)
	);

//------------------assigns -------------------------------------

		assign dac_wrfifo_ready = (dac_xfr_enable)? (!afull_fifo): 1'b0;// plz dont send anything more if the fifo is lamost full

//-----------------------------------------------------------------


//-----------------------  FX3  section -----------------------------------------------------

//----------------  registering signals wrt fx3 clk	
always @(posedge clk_fx3 or negedge reset_n)
	begin
		if(!reset_n)
		begin

			mem_wr_data <= {32{1'b0}}; 
			mem_wr_en   <= 1'b0;
			mem_wr_en_r <= 4'b0000;
			afull_fifo	<= 1'b0;
		end
		else
		begin
			mem_wr_data <= dac_wrfifo_data;
			mem_wr_en   <= dac_wrfifo_valid; 
			mem_wr_en_r <= {mem_wr_en_r[2:0],mem_wr_en};// delayed but nobody using it
			afull_fifo	<= wrusedw_a_r[6];// || wrusedw_b_r[7]; // this is a new logic could break everything ; prakash
		end
	end


//----------------------------------------------------------------------------------------	
	
	
//----------------  DDR Section or avalon MM -----------------------------------	

	always @ (*)
	begin
		n_ddr3a_mem_wr_addr = ddr3a_mem_wr_addr;
		n_ddr3a_wr_en = 1'b0;
		
		if(dac_start)		// one clock pulse must // read DDR
		begin
			n_ddr3a_wr_en   = 1'b0;
			n_ddr3a_mem_wr_addr = 32'd0;
		end
		else if(ddr3a_mem_wr_addr < (dac_size >> 4))	//32'd8192/*ddr_fall_dly[6]*/)// take care of extra bytes ; prakash
		begin
			if ((!mem_avm_m1_waitrequest) && rd_start_a)
			begin
				n_ddr3a_wr_en   = 1'b1;
				n_ddr3a_mem_wr_addr = ddr3a_mem_wr_addr + 32'd1;
			end
		end
		else
		begin
			n_ddr3a_wr_en   = 1'b0;
			n_ddr3a_mem_wr_addr = 32'd0;
		end
	end
//--------------------------------------

//------------ registering the signals on ddr clk
	always @(posedge clk_ddr or negedge reset_n)
	begin
		if(!reset_n)
		begin
			ddr3a_mem_wr_addr <= 32'd0;//{32{1'b0}};
			ddr3a_mem_wr_addr_r <= 32'd0;
			ddr3a_wr_en   <= 1'b0;
			rd_start_a <= 1'b0;
			mem_avm_m1_writedata_r <= 512'd0;
			
		end
		else
		begin
			ddr3a_mem_wr_addr <= n_ddr3a_mem_wr_addr;
			ddr3a_mem_wr_addr_r <= ddr3a_mem_wr_addr;
			ddr3a_wr_en   <= n_ddr3a_wr_en;
			rd_start_a <= !(empty_a);// | empty_b);// this could be a mistake; make it almost empty ; prakash
			mem_avm_m1_writedata_r <= mem_avm_m1_writedata_fifo;
		end
	end

	assign mem_avm_m1_address = xcvr_mode?(ddr3a_mem_wr_addr_r + 32'h2000000):ddr3a_mem_wr_addr_r;//address increment by 1 always
	assign mem_avm_m1_write = ddr3a_wr_en;			// write enable 
	assign mem_avm_m1_burstcount = BURSTCOUNT;		// 8 now
	assign mem_avm_m1_writedata = mem_avm_m1_writedata_r;

	assign		mem_avm_m1_read = 1'b0;
//--------------------------------------------------------------------------------

	//assign debug_port_tx={dac_start,1'b1,ddr3a_done_clk,dac_wrfifo_valid,mem_avm_m2_waitrequest,mem_wr_en,ddr3a_wr_en ,en_ddr3b,8'd0};

endmodule

