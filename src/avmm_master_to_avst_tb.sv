`timescale 1ps / 1ps
`define RST_DLY 1
`define AVST_READY_DLY 2
`define START_DLY 5
`define RESTART_DLY 6

`define AVST avst_sink_bfm_inst.st_sink_bfm_0
`define AVMM avmm_slave_bfm_inst.mm_slave_bfm_0

`define DATA_LEN 100

module avmm_master_to_avst_tb();

	localparam AVST_DATA_WIDTH = 32;
	localparam DATA_RATIO = 16;
	localparam AVMM_DATA_WIDTH = AVST_DATA_WIDTH*DATA_RATIO;

	localparam CLK_PERIOD = 10000; //100MHz
	localparam MEM_CLK_PERIOD = 5714; //175MHz
	
	import verbosity_pkg::*;
   	import avalon_mm_pkg::*;

	//AVST
	wire [AVST_DATA_WIDTH-1:0] avst_data;
	wire	avst_ready, avst_valid;
	
	//AVMM
	wire avmm_waitrequest, avmm_write;
	wire [AVMM_DATA_WIDTH-1:0] avmm_writedata;
	wire [31:0] avmm_address;	
	wire [3:0] avmm_burstcount;
	wire [AVMM_DATA_WIDTH-1:0] avmm_readdata;
	
	//Clock and reset
	reg avst_clk = 1'b0, avmm_clk = 1'b0;
	reg avst_rstn=1'b0;
	reg avmm_rstn = 1'b0;
	//Control
	reg start = 1'b0;
	wire busy;
	//Test data
	reg [AVST_DATA_WIDTH-1:0] data_received = 0;
	reg [AVST_DATA_WIDTH-1:0] data_received_prev = 0;
	reg [AVST_DATA_WIDTH-1:0] data = 0;
	reg [AVST_DATA_WIDTH*DATA_RATIO-1:0] data_pack = 0; 
	
	integer i=0,j=0;
	integer burstcount=0, address=0;
	
	//Clock generator
	always #(CLK_PERIOD/2) avst_clk <= ~avst_clk;
	always #(MEM_CLK_PERIOD/2) avmm_clk <= ~avmm_clk;
	
	//Reset generator
	initial begin
		repeat(`RST_DLY) @(negedge avst_clk);
		avst_rstn = 1'b1;
	end
	initial begin
		repeat(`RST_DLY) @(negedge avmm_clk);
		avmm_rstn = 1'b1;
	end
	
	//Start signal
	initial begin
		repeat(`START_DLY) @(posedge avst_clk);
		start = 1'b1;
		repeat(1) @(posedge avst_clk);
		start = 1'b0;
		@(negedge busy);
		repeat(`RESTART_DLY) @(posedge avst_clk);
		start = 1'b1;
		repeat(1) @(posedge avst_clk);
		start = 1'b0;
		@(negedge busy);
		$stop();
	end
	
	//AVMM data generator
	initial begin
		`AVMM.init();
		forever begin
			`AVMM.set_interface_wait_time($urandom%50, 0);
			@(`AVMM.signal_command_received);
			begin
				`AVMM.pop_command();
				if(`AVMM.get_command_request() == REQ_READ)
				begin
					if (((`AVMM.get_command_address()-address) != burstcount) && (`AVMM.get_command_address()!=0) )
						$display("Address  discontinuity at %d",$time);
					burstcount = `AVMM.get_command_burst_count();
					address = `AVMM.get_command_address();
					
					for(i=0;i<burstcount;i=i+1)
					begin
						for(j=0;j<DATA_RATIO;j=j+1)
						begin
							data_pack = data_pack>>AVST_DATA_WIDTH;
							data_pack[AVMM_DATA_WIDTH-1:AVMM_DATA_WIDTH-AVST_DATA_WIDTH] = data;
							data = data+1;
						end
						`AVMM.set_response_burst_size(burstcount);
						`AVMM.set_response_data(data_pack, i);
					end
					`AVMM.push_response();
				end
				else
				begin
					$display("Command shold be read");
				end
			end	
		end
	end
	
	//AVST data checker
	initial begin
		`AVST.init();
		repeat(`AVST_READY_DLY) @(posedge avst_clk);
		`AVST.set_ready(1);
		forever begin
			@(`AVST.signal_transaction_received);
			`AVST.pop_transaction();
			data_received = `AVST.get_transaction_data();
			if(data_received - data_received_prev != 1)
			begin
				$display("Data corrupted at %d", $time);
				$display("data_received %d", data_received);
				$display("data_received_prev %d", data_received_prev);
			end
			data_received_prev = data_received;
		end
	end
	
	avst_sink_bfm avst_sink_bfm_inst(
		.clk        (avst_clk),
		.reset      (!avst_rstn),
		.sink_data  (avst_data),
		.sink_valid (avst_valid), 
		.sink_ready (avst_ready)
	);
	
	avmm_slave_bfm avmm_slave_bfm_inst(
	.clk               (avmm_clk),
	.reset             (!avmm_rstn),
	.avs_writedata     (),
	.avs_burstcount    (avmm_burstcount),
	.avs_readdata      (avmm_readdata),
	.avs_address       (avmm_address),
	.avs_waitrequest   (avmm_waitrequest),
	.avs_write         (),
	.avs_read          (avmm_read),
	.avs_readdatavalid (avmm_readdatavalid)
	);

	avmm_master_to_avst avmm_master_to_avst_inst(

	.avst_clk	(avst_clk),
	.avst_rstn	(avst_rstn),
	
	.avmm_clk	(avmm_clk),
	.avmm_rstn	(avmm_rstn),
	
	//AVST source
	.avst_valid	(avst_valid),
	.avst_data	(avst_data),
	.avst_ready	(avst_ready),
	
	//AVMM master
	.avmm_waitrequest	(avmm_waitrequest),
	.avmm_read			(avmm_read),
	.avmm_readdata		(avmm_readdata),
	.avmm_readdatavalid	(avmm_readdatavalid),
	.avmm_address		(avmm_address),	
	.avmm_burstcount	(avmm_burstcount),
	
	//Control signals
	.start(start), // pulse
	.base_address(32'hff),
	.lenght(`DATA_LEN),
	.busy(busy)

);

endmodule