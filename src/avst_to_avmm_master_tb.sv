`timescale 1ps / 1ps
`define RST_DLY 3
`define AVST_DATA_DLY 5
`define START_DLY 7

`define AVST avst_source_bfm_inst.st_source_bfm_0
`define AVMM avmm_slave_bfm_inst.mm_slave_bfm_0

`define DATA_LEN 4096

module avst_to_avmm_master_tb();
	
	localparam AVST_DATA_WIDTH = 32;
	localparam DATA_RATIO = 16;
	localparam AVMM_DATA_WIDTH = AVST_DATA_WIDTH*DATA_RATIO;
		
	localparam CLK_PERIOD = 10000; //100MHz
	localparam MEM_CLK_PERIOD = 5714; //175MHz
	
	import verbosity_pkg::*;
   	import avalon_mm_pkg::*;
	
	//AVST
	wire [AVST_DATA_WIDTH-1:0] avst_data;
	wire	avst_ready, avst_valid;
	
	//AVMM
	wire avmm_waitrequest, avmm_write;
	wire [AVMM_DATA_WIDTH-1:0] avmm_writedata;
	wire [31:0] avmm_address;	
	wire [3:0] avmm_burstcount;
	
	//Clock and reset
	reg clk = 1'b0, mem_clk = 1'b0;
	reg rstn=1'b0;
	reg mem_rstn = 1'b0;
	
	
	reg start = 1'b0;
	
	reg [AVST_DATA_WIDTH-1:0] data_in = 0; 
	
	integer i=0,j=0;	
	string message;
	
	integer burstcount=0, address=0;
	reg [AVMM_DATA_WIDTH-1:0] mem_data;
	reg [AVST_DATA_WIDTH-1:0] mem_data_split, mem_data_split_prev;
	
	//Clock generator
	always #(CLK_PERIOD/2) clk <= ~clk;
	always #(MEM_CLK_PERIOD/2) mem_clk <= ~mem_clk;
	
	//Reset generator
	initial begin
		repeat(`RST_DLY) @(negedge clk);
		rstn = 1'b1;
	end
	initial begin
		repeat(`RST_DLY) @(negedge mem_clk);
		mem_rstn = 1'b1;
	end
	
	//Start signal
	initial begin
		repeat(`START_DLY) @(posedge clk);
		start = 1'b1;
		repeat(1) @(posedge clk);
		start = 1'b0;
	end
	//AVST data generator
	initial begin
		`AVST.init();
		repeat(`AVST_DATA_DLY) @(negedge clk);
		forever begin
			@(posedge clk);
			if(`AVST.get_src_ready())
			begin
				`AVST.set_transaction_data(data_in);
				`AVST.push_transaction();
				if (data_in > `DATA_LEN*2)
					$stop;
				data_in = data_in+1;
			end	
		end
	end

	//Memory data and address checker
	initial begin
		`AVMM.init();
		`AVMM.set_interface_wait_time(3,0);
		forever begin
			@(`AVMM.signal_command_received);
			`AVMM.set_interface_wait_time($urandom%200,0);
			`AVMM.pop_command();
			burstcount = `AVMM.get_command_burst_count();
			if (((`AVMM.get_command_address()-address) != burstcount) && (`AVMM.get_command_address()!=0) )
				$display("Address  discontinuity at %d",$time);	
			address = `AVMM.get_command_address();
			for (i=0;i<burstcount;i=i+1) 
			begin
				mem_data = `AVMM.get_command_data(i);
				//$display("mem_data %b", mem_data);
				for(j=0;j<DATA_RATIO;j=j+1)
				begin	
					mem_data_split = mem_data[AVST_DATA_WIDTH-1:0];
					mem_data = mem_data>>AVST_DATA_WIDTH;
					//$display("mem_data_split %d", mem_data_split);
					//$display("mem_data_split_prev %d", mem_data_split_prev);
					if(mem_data_split != mem_data_split_prev+1)
					begin
						$display("Data corrupted at %d", $time);
						$display("mem_data_split %d", mem_data_split);
						$display("mem_data_split_prev %d", mem_data_split_prev);	
					end
					mem_data_split_prev = mem_data_split;
				end
			end
		end
				
	end

avst_to_avmm_master 
	#(
	.AVST_DATA_WIDTH(AVST_DATA_WIDTH), 
	.AVMM_DATA_WIDTH(AVST_DATA_WIDTH*DATA_RATIO)
	)
	avst_to_avmm_master_inst
	(

	.avst_clk	(clk),
	.avst_rstn	(rstn),
	
	.avmm_clk	(mem_clk),
	.avmm_rstn	(mem_rstn),
	
	//AVST sink
	.avst_valid	(avst_valid),
	.avst_data	(avst_data),
	.avst_ready	(avst_ready),
	
	//AVMM master
	.avmm_waitrequest	(avmm_waitrequest),
	.avmm_write			(avmm_write),
	.avmm_writedata		(avmm_writedata),
	.avmm_address		(avmm_address),	
	.avmm_burstcount	(avmm_burstcount),
	
	//Control signals
	.start			(start), // pulse
	.base_address	(0),
	.lenght			(100),
	.busy			()

);

avst_source_bfm avst_source_bfm_inst(
	.clk       (clk),
	.reset     (!rstn),
	.src_data  (avst_data),
	.src_valid (avst_valid),
	.src_ready (avst_ready)  
);

avmm_slave_bfm avmm_slave_bfm_inst(
	.clk               (mem_clk),
	.reset             (!mem_rstn),
	.avs_writedata     (avmm_writedata),
	.avs_burstcount    (avmm_burstcount),
	.avs_readdata      (),
	.avs_address       (avmm_address),
	.avs_waitrequest   (avmm_waitrequest),
	.avs_write         (avmm_write),
	.avs_read          (),
	.avs_readdatavalid ()
	);

endmodule
