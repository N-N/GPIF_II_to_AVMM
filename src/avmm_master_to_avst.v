//Avalon memory mapped master (burst read) to streaming source converter
//N_N
//Control signals are synchroneous to avst_clk

module avmm_master_to_avst #( parameter 	AVST_DATA_WIDTH = 32, 
								AVMM_ADDR_WIDTH = 32,
								AVMM_DATA_WIDTH = 512,
								AVMM_BURSTCOUNT_WIDTH = 4,
								AVMM_BURSTCOUNT = 8,//Must be power of 2
								DATA_CNT_WIDTH = 32,
								FIFO_DEPTH = 16,//In wr words, must be power of 2
								FIFO_WR_LEVEL = 2 //Write fifo if below
								)
(

	input wire 			avst_clk,
	input wire 			avst_rstn,
	
	input wire 			avmm_clk,
	input wire 			avmm_rstn,
	
	//AVST source
	output reg							avst_valid,
	output wire [AVST_DATA_WIDTH-1:0]	avst_data,
	input wire							avst_ready,
	
	//AVMM master
	input wire 								avmm_waitrequest,
	output reg 								avmm_read,
	input wire [AVMM_DATA_WIDTH-1:0] 		avmm_readdata,
	input wire 								avmm_readdatavalid,
	output reg [AVMM_ADDR_WIDTH-1:0] 		avmm_address,	
	output reg [AVMM_BURSTCOUNT_WIDTH-1:0]	avmm_burstcount,
	
	//Control signals
	//Must be synchroneous to avst_clk
	input wire 							start, // pulse
	input wire [AVMM_ADDR_WIDTH-1:0]	base_address,
	input wire [31:0]					lenght,
	output reg 							busy

);

//AVST state machine
parameter WAIT_AVMM_READY = 'd1, AVST_WRITE = 'd2;
reg [1:0] avst_state, n_avst_state;
//AVMM state machine
parameter IDLE = 'd0, AVMM_ASSERT_READ  ='d1, AVMM_READ = 'd2;
reg [1:0] avmm_state, n_avmm_state;
reg n_avst_valid;


//AVMM
reg [AVMM_ADDR_WIDTH-1:0] n_avmm_address;
reg [DATA_CNT_WIDTH-1:0] n_avmm_data_cnt, avmm_data_cnt;
reg n_avmm_read;
reg [AVMM_BURSTCOUNT_WIDTH-1:0] n_avmm_burstcount;
reg [AVMM_BURSTCOUNT_WIDTH-1:0] n_avmm_burst_cnt, avmm_burst_cnt; //Burst counter

//Control
reg n_busy, busy_avmm_clk_r;
wire busy_avmm_clk;

reg avst_en, n_avst_en;

wire avmm_busy_avst_clk; 
reg n_avmm_busy, avmm_busy;

reg avmm_rest_flag, n_avmm_rest_flag;

//Rest from devision by AVMM_BURSTCOUNT
wire [$clog2(AVMM_BURSTCOUNT)-1:0] rest_lenght; 
assign rest_lenght = lenght[$clog2(AVMM_BURSTCOUNT)-1:0];

//FIFO
wire fifo_rdempty, fifo_aclr;
wire [$clog2(FIFO_DEPTH)-1:0] fifo_wrusedw;
reg fifo_wr, fifo_clr, n_fifo_clr;
wire fifo_rd;

assign fifo_rd = avst_en && avst_ready && !fifo_rdempty;
	
always @ (*)
begin
	n_avst_state = avst_state;
	n_busy = busy;
	n_avst_en = avst_en;
	n_avst_valid = fifo_rd;
	
	case (avst_state)
		IDLE:
		begin
			if (start)
			begin
				n_busy = 1'b1;
				n_avst_state = WAIT_AVMM_READY;
			end	
		end
		
		WAIT_AVMM_READY:
		begin
			if(avmm_busy_avst_clk)
			begin
				n_avst_state = AVST_WRITE;
				n_avst_en = 1'b1;
			end
		end
		
		AVST_WRITE:
		begin
			//Stop condition
			if( !avmm_busy_avst_clk && fifo_rdempty)
			begin
				n_busy = 1'b0;
				n_avst_en = 1'b0;
				n_avst_state = IDLE;
			end	
		end
		
	endcase		
end

always @(posedge avst_clk or negedge avst_rstn)
begin
	if(!avst_rstn)
	begin
		avst_state	<=	IDLE;
		busy	<=	1'b0;
		avst_en <=	1'b0;
		avst_valid <= 1'b0;
		
	end
	else
	begin
		avst_state	<=	n_avst_state;
		busy	<=	n_busy;
		avst_en <= n_avst_en;
		avst_valid <= n_avst_valid;
	end
end	

//Synchronizers
data_sync sync0(
	.data		(busy),
	.dest_clk	(avmm_clk),
	.dest_rstn	(avmm_rstn),
	.data_sync	(busy_avmm_clk)
);

data_sync sync1(
	.data		(avmm_busy),
	.dest_clk	(avst_clk),
	.dest_rstn	(avst_rstn),
	.data_sync	(avmm_busy_avst_clk)
);

//FIFO
dcfifo_mixed_widths	wr_fifo (
				.aclr 		(fifo_aclr),
				.data 		(avmm_readdata),
				.rdclk 		(avst_clk),
				.rdreq 		(fifo_rd),
				.wrclk 		(avmm_clk),
				.wrreq 		(fifo_wr),
				.q 			(avst_data),
				.rdempty 	(fifo_rdempty),
				.wrusedw 	(fifo_wrusedw),
				.eccstatus 	(),
				.rdfull 	(),
				.rdusedw 	(),
				.wrempty 	(),
				.wrfull 	());
	defparam
		wr_fifo.intended_device_family = "Arria V GZ",
		wr_fifo.lpm_numwords = FIFO_DEPTH,
		wr_fifo.lpm_showahead = "OFF",
		wr_fifo.lpm_type = "dcfifo_mixed_widths",
		wr_fifo.lpm_width = AVMM_DATA_WIDTH,
		wr_fifo.lpm_widthu = $clog2(FIFO_DEPTH),
		wr_fifo.lpm_widthu_r = $clog2(FIFO_DEPTH*AVMM_DATA_WIDTH/AVST_DATA_WIDTH),
		wr_fifo.lpm_width_r = AVST_DATA_WIDTH,
		wr_fifo.overflow_checking = "ON",
		wr_fifo.rdsync_delaypipe = 5,
		wr_fifo.read_aclr_synch = "OFF",
		wr_fifo.underflow_checking = "ON",
		wr_fifo.use_eab = "ON",
		wr_fifo.write_aclr_synch = "OFF",
		wr_fifo.wrsync_delaypipe = 5;

//Asynchroneous clear signal
assign fifo_aclr = !avmm_rstn || fifo_clr;

always @(*)
begin
	n_avmm_state = avmm_state;
	n_avmm_data_cnt = avmm_data_cnt;
	n_avmm_address = avmm_address;
	n_avmm_burstcount = avmm_burstcount;
	n_avmm_burst_cnt = avmm_burst_cnt;
	n_avmm_read = avmm_read;
	n_avmm_busy = avmm_busy;
	n_avmm_rest_flag = avmm_rest_flag;
	n_fifo_clr = 1'b0;
	fifo_wr=1'b0;
	
	case (avmm_state)
		IDLE:
		begin
			if (busy_avmm_clk && !busy_avmm_clk_r)//Posedge busy_avmm_clk
			begin
				n_fifo_clr = 1'b1;
				n_avmm_burstcount = AVMM_BURSTCOUNT;
				n_avmm_address = base_address;
				n_avmm_data_cnt = {lenght[DATA_CNT_WIDTH-1:$clog2(AVMM_BURSTCOUNT)],  {$clog2(AVMM_BURSTCOUNT){1'b0}} };
				n_avmm_busy = 1'b1;
				n_avmm_state = AVMM_ASSERT_READ;
			end
			else
			begin
				n_avmm_busy = 1'b0;
			end
		end
		
		AVMM_ASSERT_READ:
		begin
			if (fifo_wrusedw < FIFO_WR_LEVEL)
			begin
				n_avmm_read = 1'b1;
				n_avmm_burst_cnt = avmm_burstcount;
				n_avmm_state = AVMM_READ;
			end	
		end		

		AVMM_READ:
		begin
			if(!avmm_waitrequest)
				n_avmm_read = 1'b0;
			if(avmm_readdatavalid)
			begin
				fifo_wr = 1'b1;
				n_avmm_address = avmm_address + 'd1;
				n_avmm_data_cnt = avmm_data_cnt-'d1;
				n_avmm_burst_cnt = avmm_burst_cnt-'d1;
				if(avmm_burst_cnt == 'd1)
				begin
					if (avmm_data_cnt == 'd1)
					begin
						if( avmm_rest_flag )
						begin
							//If burstcount is less than AVMM_BURSTCOUNT it means we red the rest already
							n_avmm_rest_flag = 1'b0;
							n_avmm_state = IDLE;
						end
						else
						begin
							if( rest_lenght == 'd0 )
							begin
								//If lenght is devideble by AVMM_BURSTCOUNT
								n_avmm_state = IDLE;
							end
							else
							begin	
								//If lenght is not devideble by AVMM_BURSTCOUNT
								n_avmm_data_cnt = { {(DATA_CNT_WIDTH-$clog2(AVMM_BURSTCOUNT)){1'b0}}, rest_lenght};
								n_avmm_burstcount = { {(AVMM_BURSTCOUNT_WIDTH-$clog2(AVMM_BURSTCOUNT)){1'b0}}, rest_lenght};
								n_avmm_rest_flag = 1'b1;
								n_avmm_state = AVMM_ASSERT_READ;
							end
						end
					end
					else
					begin
						n_avmm_state = AVMM_ASSERT_READ;
					end	
				end	
			end	
		end
		
	endcase
end

always @(posedge avmm_clk or negedge avmm_rstn)
begin
	if (!avst_rstn)
	begin
		avmm_state		<= IDLE;
		avmm_read 		<= 1'b0;
		avmm_data_cnt	<= 'd0;
		avmm_address	<= 'd0;
		avmm_busy		<= 1'b0;
		avmm_burstcount <= 'd0;
		avmm_burst_cnt	<=1'b0;
		fifo_clr		<= 1'b0;
		busy_avmm_clk_r <=1'b0;
		avmm_rest_flag <= 1'b0;
	end
	else
	begin
		busy_avmm_clk_r <= busy_avmm_clk;
		avmm_state		<=	n_avmm_state;
		avmm_read 		<= 	n_avmm_read;
		avmm_data_cnt	<= 	n_avmm_data_cnt;
		avmm_address	<=	n_avmm_address;
		avmm_busy		<=	n_avmm_busy;
		avmm_burstcount <= 	n_avmm_burstcount;
		avmm_burst_cnt	<= 	n_avmm_burst_cnt;
		fifo_clr <= n_fifo_clr;
		avmm_rest_flag <= n_avmm_rest_flag;
	end
end

endmodule