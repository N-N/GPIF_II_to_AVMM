//////////////////////////////////////////////////////////////////////////////////
// Company: 		Soliton Technologies
// Engineer: 		Prakash/Gokul
// 
// Create Date:   23/08/2014 
// Design Name: 	USB3 FX3
// Module Name:   fx3_main 
// Project Name: 	TSW14J56 _USB3 project
// Target Devices: Arria V
// Tool versions:  Quartus 14
// Description: 
//
// Dependencies: 
//
// Revision: 		0.0
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

/*
*For ADC mode.. adc start pulse has to be applied. The pulse will make adc_xfr_enable High. Which activated the ddr module..
*and also the state machine. 
*
*
*
*
*/

module fx3interface
(
	input					clk_0ph,			// clock input from crystal 100MHz
	input					clk_180ph,
	input 				reset_n,		// reset active low	

// DDR Read FIFO signals 
   input  wire                  	adc_rdfifo_valid,  // change this to valid and change the logic accordingly; Prakash
	input  wire [31:0]       		adc_rdfifo_data,// delayed by 2 clocks
	output wire                 	adc_rdfifo_ready,
	
// DDR write FIFO signals
   output  wire                  dac_wrfifo_valid,	
	output  wire 		[31:0]      dac_wrfifo_data,
	input   wire						dac_wrfifo_ready, // need to change the logic accordingly ; prakash
	
//	input 				sw_3,			// fx3_wr_button
//	input 				sw_2,			// fx3_rd_button
	input 				sw_5,			// reset_n
	
	output	[7:0]		LEDs,
//	output				debug_1,
//------  FX3 related signals -----------------------
	output			fx3_pclk,  	// clock to fx3
	inout	[31:0] 	fx3_data,  	// data bidirectional signal ended
	output	[1:0]		fx3_adrs,	// adrs pins 2 bit to selct read write threads
	input				fx3_flagA,  // FX3 RX full flag
	input				fx3_flagB,	// FX3 RX partial flag delay 10 clocks
	input				fx3_flagC, 	// FX3 TX full flag
	input				fx3_flagD, 	// FX3 TX partial flag delay 6 clocks
	
	output			fx3_slcs_n,	//	Active low chip select.. initiates start of transfer
	output			fx3_slwr_n, // Write enable to FX3 RX FIFO
	output			fx3_slrd_n, // Read enable to FX3 TX FIFO
	output			fx3_sloe_n, // Output enable for the biderectional bus.. 1=> FX3 input 0=> FX3 drives the data bus
	output			fx3_pktend_n,		// assert this signal to send small chunks of data. not used now hold it High
	
//-------------------------------------------------	
	
	input  wire 		adc_start, // change this logic idiot
	input	wire			dac_start,
	input 		[31:0]	adc_size,
	input 		[31:0] 	dac_size,
	output			adc_xfr_enable,		//assign not performed; Gokul
	output			dac_xfr_enable			//assign not performed; Gokul
);


//	assign reset_n 			= sw_5;		// active low reset
	
	reg 		slcs_r, sloe_r ;
	reg	[1:0]	faddr;	//FX3 R/W thread Selection Address Bus
	
	reg			flag_a_r;
	reg			flag_b_r;
	reg			flag_c_r;
	reg			flag_d_r;
	
	reg adc_xfr_enable_r;
	reg dac_xfr_enable_r;
	//reg [31:0] ramp_data;
		
	reg [2:0]current_stream_in_state;
	reg [2:0]next_stream_in_state;


	//parameters for StreamIN mode state machine
	parameter [2:0] stream_in_idle                    = 3'd0;
	parameter [2:0] stream_in_wait_flagb              = 3'd1;
	parameter [2:0] stream_in_write                   = 3'd2;
	parameter [2:0] stream_in_write_wr_delay          = 3'd3;
	
	reg [2:0]current_stream_out_state;
	reg [2:0]next_stream_out_state;

	//parameters for StreamOUT mode state machine
	parameter [2:0] stream_out_idle                   = 3'd0;
	parameter [2:0] stream_out_flagc_rcvd             = 3'd1;
	parameter [2:0] stream_out_wait_flagd             = 3'd2;
	parameter [2:0] stream_out_read                   = 3'd3;
	parameter [2:0] stream_out_read_rd_and_oe_delay   = 3'd4;
	parameter [2:0] stream_out_read_oe_delay          = 3'd5;


	reg	[31:0] fx3_data_r;
	wire slrd_streamOUT_n;
	wire sloe_streamOUT_n;
	wire slwr_streamIN_n;

	reg rd_oe_delay_cnt;
	reg [1:0]	oe_delay_cnt;
	reg slwr_streamIN_d1_;
	
	reg [7:0] slwr_r;
	reg [7:0] slrd_r;

	
//ddr is used to send out the clk(ODDR2 instantiation)
//ALTDDIO_OUT
//--------------------------------------------------

ddr_out inst_ddr_to_send_clk_to_fx3                       
        ( 
	.datain_h(1'b0),
	.datain_l(1'b1),
	.outclock(clk_0ph),
	.dataout(fx3_pclk) 
	);

//---------------------------------------------------	
	
//---- Registering inputs at posedge of the clock send to fx3 -------------------	


	always @(posedge clk_0ph or negedge reset_n)
	begin	
		if(!reset_n)
		begin
			flag_a_r <= 1'd0;
			flag_b_r <= 1'd0;
			flag_c_r <= 1'd0;
			flag_d_r <= 1'd0;
			fx3_data_r	<= 32'hDEAD0001;
		end else
		begin	
			flag_a_r <= fx3_flagA;
			flag_b_r <= fx3_flagB;
			flag_c_r <= fx3_flagC;
			flag_d_r <= fx3_flagD;
			fx3_data_r	<= fx3_data;
		end	
	end
//-----------------------------------------------------------------------	

//------- counters for stopping and starting acquisition and generation---------------------------------------	
	reg [31:0] adc_size_counter;
	reg [31:0] dac_size_counter;
	
	always @(posedge clk_0ph or negedge reset_n)	
	begin
		if(!reset_n)
			adc_size_counter <= 32'd0;
		else if(adc_start)// one clock pulse must // read DDR
			adc_size_counter <= adc_size;
		else if(adc_xfr_enable_r & adc_rdfifo_valid)//check; gokul
			adc_size_counter <= adc_size_counter - 32'd1;	
		else if(!adc_xfr_enable_r)
			adc_size_counter <= 32'd0;	//adc_size;
	end
	
	always @(posedge clk_0ph or negedge reset_n)	
	begin
		if(!reset_n)
			adc_xfr_enable_r <= 1'b0;
		else if(adc_start & (!dac_xfr_enable_r))// one clock pulse must // read DDR
			adc_xfr_enable_r <= 1'b1;
		else if(adc_size_counter ==32'd1) 
			adc_xfr_enable_r <= 1'b0;			
	end	
	
	always @(posedge clk_0ph or negedge reset_n)	
	begin
		if(!reset_n)
			dac_size_counter <= 32'h0;
		else if(dac_start) // one clock pulse must // Write to DDR
			dac_size_counter <= dac_size;
		else if(dac_xfr_enable_r & dac_wrfifo_valid)
			dac_size_counter <= dac_size_counter - 32'h1;	
		else if(!dac_xfr_enable_r)
			dac_size_counter <= 32'h0;	//dac_size;
	end
	
	always @(posedge clk_0ph or negedge reset_n)	
	begin
		if(!reset_n)
			dac_xfr_enable_r <= 1'b0;
		else if(dac_start & (!adc_xfr_enable_r))// one clock pulse must // read DDR
			dac_xfr_enable_r <= 1'b1;
		else if((dac_size_counter < 32'h1))
			dac_xfr_enable_r <= 1'b0;			
	end
//------------------------------------------------------------------------	

//------------ stream in State machine ADC-------------------------------------

	//streamIN mode state machine
	always @(posedge clk_0ph or negedge reset_n)	
	begin
		if(!reset_n)	 
			current_stream_in_state <= stream_in_idle;
		else 			
			current_stream_in_state <= next_stream_in_state;
	end


	//StreamIN mode state machine combo
	always @(*)	
	begin
		next_stream_in_state = current_stream_in_state;
		
		case(current_stream_in_state)
		
		stream_in_idle:
		begin	
			if((flag_a_r == 1'b1) & (adc_xfr_enable_r))	
				next_stream_in_state = stream_in_wait_flagb; 
			else 	
				next_stream_in_state = stream_in_idle;
		end
		
		stream_in_wait_flagb :	
		begin
			if(adc_xfr_enable_r) begin				
				if(flag_b_r == 1'b1)
					next_stream_in_state = stream_in_write; 
				else
					next_stream_in_state = stream_in_wait_flagb; 
			end
			else
				next_stream_in_state = stream_in_idle;
		end
		
		stream_in_write:	
		begin
			if(adc_xfr_enable_r) begin
				if(flag_b_r == 1'b0)
					next_stream_in_state = stream_in_write_wr_delay;
				else 	
					next_stream_in_state = stream_in_write;
			end
			else
				next_stream_in_state = stream_in_write_wr_delay;
		end
		
	  stream_in_write_wr_delay:	
			begin
				next_stream_in_state = stream_in_idle;
		end
		
		endcase
	end	
	
	//end of stream in mode state machine
	
	assign slwr_streamIN_n = ((current_stream_in_state == stream_in_write)) ? 1'b0 : 1'b1;
	
//----------------------------------------------------------

//------------------ ADC FIFO Data Controls -----------------------

	always @(posedge clk_0ph or negedge reset_n)		//DAC FIFO Controls
	begin
		if(!reset_n)
			slwr_r <= 8'hFF;
		else
			slwr_r <= {slwr_r[6:0],slwr_streamIN_n};
	end

assign adc_rdfifo_ready = (!slwr_r[0]);	//original 0; Gokul
	
//-----------------------------------------------------------		

	
	
//--------------- stream out state machine DAC--------------------------------

//----------streamOUT mode state machine-----------------
always @(posedge clk_0ph or negedge reset_n)	
begin
	if(!reset_n)	 
		current_stream_out_state <= stream_out_idle;
	else 	
		current_stream_out_state <= next_stream_out_state;
end

//-----------steamOUT mode state machine combo----------
always @(*)	
begin
	next_stream_out_state = current_stream_out_state;
	
	case(current_stream_out_state)
	
	stream_out_idle:	
	begin
		if((flag_c_r == 1'b1)&(dac_xfr_enable_r == 1'b1))	
			next_stream_out_state = stream_out_flagc_rcvd;		//stream_out_flagc_rcvd;
		else 	
			next_stream_out_state = stream_out_idle;
	end

   stream_out_flagc_rcvd:
	begin
		next_stream_out_state = stream_out_wait_flagd;
	end
	
	stream_out_wait_flagd:
	begin
		if(dac_xfr_enable_r == 1'b1) 
		begin
			if(flag_d_r == 1'b1)	
				next_stream_out_state = stream_out_read;
			else 	
				next_stream_out_state = stream_out_wait_flagd;
		end
		else
			next_stream_out_state = stream_out_idle;
	end

	stream_out_read :	
	begin
		 if((flag_d_r == 1'b1)&(dac_xfr_enable_r == 1'b1))	
			next_stream_out_state = stream_out_read;	//data received here
		else 	
			next_stream_out_state = stream_out_read_rd_and_oe_delay;
	end
	
	stream_out_read_rd_and_oe_delay :
 	begin
		if(rd_oe_delay_cnt == 1'd0)	  // only 1 delay
			next_stream_out_state = stream_out_read_oe_delay;
		else 	
			next_stream_out_state = stream_out_read_rd_and_oe_delay;	//last two or three cycle data may be received here -Gokul
	end
   
	stream_out_read_oe_delay : 
	begin
		if(oe_delay_cnt == 2'd0)	  // currently delay of 2
			next_stream_out_state = stream_out_idle;
		else 	
			next_stream_out_state = stream_out_read_oe_delay;
	end
	
	endcase
end

assign slrd_streamOUT_n = ((current_stream_out_state == stream_out_read) | (current_stream_out_state == stream_out_read_rd_and_oe_delay)) ? (!dac_wrfifo_ready) : 1'b1;
//assign sloe_streamOUT_n = ((current_stream_out_state == stream_out_read) | (current_stream_out_state == stream_out_read_rd_and_oe_delay) | (current_stream_out_state == stream_out_read_oe_delay)) ? 1'b0 : 1'b1; //(!dac_wrfifo_ready) : 1'b1;

//-----------counter to delay the read and output enable signal-----------
always @(posedge clk_0ph or negedge reset_n)	
begin
	if(!reset_n)	 
		rd_oe_delay_cnt <= 1'd0;
	else if(current_stream_out_state == stream_out_read) 	
		rd_oe_delay_cnt <= 1'd1;
   else if((current_stream_out_state == stream_out_read_rd_and_oe_delay) & (rd_oe_delay_cnt > 1'd0))	
		rd_oe_delay_cnt <= rd_oe_delay_cnt - 1'd1;
	else 	
		rd_oe_delay_cnt <= rd_oe_delay_cnt;
end

//-------------Counter to delay the OUTPUT Enable(oe) signal---------------
always @(posedge clk_0ph or negedge reset_n)	
begin
	if(!reset_n)	 
		oe_delay_cnt <= 2'd0;
	else if(current_stream_out_state == stream_out_read_rd_and_oe_delay)
		oe_delay_cnt <= 2'd2;
	else if((current_stream_out_state == stream_out_read_oe_delay) & (oe_delay_cnt > 2'd0))	
		oe_delay_cnt <= oe_delay_cnt - 2'd1;
	else 	
		oe_delay_cnt <= oe_delay_cnt;
end

//-------------------------------------------------------------------------	

//------------------ DAC FIFO Data Controls -------------------------------

	always @(posedge clk_0ph or negedge reset_n)		//DAC FIFO Controls
	begin
		if(!reset_n)
			slrd_r <= 8'hFF;	//8'hFF;
		else
			slrd_r <= {slrd_r[6:0],slrd_streamOUT_n};		//slrd_streamOUT_n};
	end

	assign dac_wrfifo_valid = (!slrd_r[2]);		//0,1 - two cycle latency	//original 2
	assign dac_wrfifo_data =/* (!slrd_r[2])?*/ fx3_data_r;// : 32'hDEAD7001;// write internal fifo daa invalid

//------------------------------------------------------------------------	

//------------FX3 Address Setting for Selecting the R/W Thread------------
	always @(posedge clk_0ph or negedge reset_n)	
	begin	
		if(!reset_n)
			faddr <= 2'b11; // handle any delays if necessary; prakash
		else if(adc_start || adc_xfr_enable_r)
			faddr <= 2'b00;		//Selecting Thread 0 For FX3 Write Operation
		else if(dac_start || dac_xfr_enable_r)
			faddr <= 2'b11;	//Selecting Thread 3 For FX3 Read Operation
	end
	
//-------------------------------------------------------------------------

//----------------   signal assignements to  FX3 --------------------------
	
	//assign fx3_data  		= (adc_xfr_enable_r) ? ramp_data : 32'dz;//Change it, for testing; Gokul
	assign fx3_data  		= (adc_xfr_enable_r) ? adc_rdfifo_data :32'dz;// 32'dz : adc_rdfifo_data;// after flagb goes low there are 4 clcoks for which data will be coming 
	assign fx3_slwr_n   	= (adc_xfr_enable_r)? (!(adc_rdfifo_valid)) : 1'b1;  	
	
	//output signal assignment 
	assign fx3_slrd_n   	= slrd_streamOUT_n;	//slrd_streamOUT_n;

	assign fx3_adrs  		= faddr;		// 11 for OUT  00 for IN
	assign fx3_sloe_n   	= !(dac_xfr_enable_r); //((!dac_xfr_enable_r)&(sloe_streamOUT_n));
	assign fx3_slcs_n   	= 1'b0;
	assign fx3_pktend_n 	= 1'b1;
	
	assign adc_xfr_enable = adc_xfr_enable_r;
	assign dac_xfr_enable = dac_xfr_enable_r;
	
	//assign fx3_pclk = clk_0ph;
	
	assign LEDs = (reset_n)? {fx3_flagA,fx3_flagB,fx3_flagC,fx3_flagD,slwr_streamIN_n,adc_xfr_enable_r,slrd_streamOUT_n,dac_xfr_enable_r}:8'b11001100;

//------------------------------------------------------------------------

endmodule
  