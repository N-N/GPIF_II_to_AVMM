
//////////////////////////////////////////////////////////////////////////////////
// Company: 		Soliton Technologies
// Engineer: 		Prakash/Gokul
// 
// Create Date:   23/08/2014 
// Design Name: 	USB3 FX3
// Module Name:   fx3_main 
// Project Name: 	TSW14J56 _USB3 project
// Target Devices: Arria V
// Tool versions:  Quartus 14
// Description: 
//
// Dependencies: 
//
// Revision: 		0.0
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


module fx3_main(
	input 	clk_sys,
	input	clk_ddr,
	output	fx3_pclk,  	// clock to fx3
	input 	reset_n,
	
//------  FX3 related signals -----------------------
	
	inout	[31:0] 	fx3_data,  	// data bidirectional signal ended
	output	[1:0]	fx3_adrs,	// adrs pins 2 bit to selct read write threads
	input			fx3_flagA,  // FX3 RX full flag
	input			fx3_flagB,	// FX3 RX partial flag delay 10 clocks
	input			fx3_flagC, 	// FX3 TX full flag
	input			fx3_flagD, 	// FX3 TX partial flag delay 6 clocks
	
	output			fx3_slcs_n,	//	Active low chip select.. initiates start of transfer
	output			fx3_slwr_n, // Write enable to FX3 RX FIFO
	output			fx3_slrd_n, // Read enable to FX3 TX FIFO
	output			fx3_sloe_n, // Output enable for the biderectional bus.. 1=> FX3 input 0=> FX3 drives the data bus
	output			fx3_pktend_n,		// assert this signal to send small chunks of data. not used now hold it High
	
//-------- DDR interface signals ----------------------	
// Avalon Memory Mapped Master
	output wire				mem_read,
	input wire				mem_waitrequest,
	input wire	[511:0]		mem_readdata,
	output wire				mem_write,
	output wire	[511:0]		mem_writedata,
	input wire				mem_readdatavalid,
	output wire	[31:0]		mem_address,	
	output wire	[3:0]		mem_burstcount,

	//Control interface
	input 	wire			csr_write,
	input	wire			csr_read,
	input	wire  [15:0]	csr_address,
 	input	wire  [31:0]	csr_datain,
	input 	wire 			csr_chipselect,
	output	wire			csr_readdatavalid,
	output	wire  [31:0]	csr_dataout,
	
);


//-----------   clock section -------------------------
wire clk_100M_0ph;
wire clk_100M_180ph;
wire clk_100M_90ph;
wire locked;

wire clk_fx3 = clk_sys;
wire resetn = reset_n;

//----------- Cypress FX3 interface driver ---------------

wire	adc_rdfifo_valid;
wire	[31:0] adc_rdfifo_data;
wire	adc_rdfifo_ready;

wire	dac_wrfifo_valid;
wire	[31:0]	dac_wrfifo_data;
wire	dac_wrfifo_ready;

//wire	adc_start;
//wire	dac_start;
//wire	softreset;
wire	[31:0]	adc_size;
wire	[31:0]	dac_size;

assign adc_size=size;
assign dac_size=size;

wire dac_xfr_enable;
wire adc_xfr_enable;

fx3interface fx3driver
(
	.clk_0ph(clk_fx3),			// clock input from crystal 100MHz
	.clk_180ph(),
	.reset_n(resetn),		// reset active low	

// DDR Read FIFO signals 
	.adc_rdfifo_valid(adc_rdfifo_valid),
	.adc_rdfifo_data(adc_rdfifo_data),// delayed by 2 clocks
	.adc_rdfifo_ready(adc_rdfifo_ready),
	
// DDR write FIFO signals
	.dac_wrfifo_valid(dac_wrfifo_valid),
	.dac_wrfifo_data(dac_wrfifo_data),
	.dac_wrfifo_ready(dac_wrfifo_ready),
	
//	input 				sw_3,			// fx3_wr_button
//	input 				sw_2,			// fx3_rd_button
	.sw_5(sw_5),			// reset_n
	
	.LEDs(leds),	//8b
//	output				debug_1,
//------  FX3 related signals -----------------------
	.fx3_pclk(fx3_pclk),  	// clock to fx3
	.fx3_data(fx3_data),  	// data bidirectional signal ended	//32b
	.fx3_adrs(fx3_adrs),	// adrs pins 2 bit to selct read write threads	//2b
	.fx3_flagA(fx3_flagA),  // FX3 RX full flag
	.fx3_flagB(fx3_flagB),	// FX3 RX partial flag delay 10 clocks
	.fx3_flagC(fx3_flagC), 	// FX3 TX full flag
	.fx3_flagD(fx3_flagD), 	// FX3 TX partial flag delay 6 clocks
	
	.fx3_slcs_n(fx3_slcs_n),	//	Active low chip select.. initiates start of transfer
	.fx3_slwr_n(fx3_slwr_n), // Write enable to FX3 RX FIFO
	.fx3_slrd_n(fx3_slrd_n), // Read enable to FX3 TX FIFO
	.fx3_sloe_n(fx3_sloe_n), // Output enable for the biderectional bus.. 1=> FX3 input 0=> FX3 drives the data bus
	.fx3_pktend_n(fx3_pktend_n),		// assert this signal to send small chunks of data. not used now hold it High
	
//-------------------------------------------------	
	.adc_start(adc_start),
	.dac_start(dac_start),
	.adc_size(adc_size),	//32b
	.dac_size(dac_size),	//32b
	.adc_xfr_enable(adc_xfr_enable),
	.dac_xfr_enable(dac_xfr_enable)	
);


//---------------------------------------------------------


//-----------  FX3 to DDR (DDR write) ---------------------------------
wr_master fx3toddr #(
	.ADDR_WIDTH(32),
	.AVST_DATA_WIDTH(32),
	.AVMM_DATA_WIDTH(512)
	)
(
	.avst_clk			(clk_fx3),
	.avmm_clk			(clk_ddr),
	.reset_n			(resetn),
	
	//FX3
	.wr_valid			(dac_wrfifo_valid),
	.wr_data			(dac_wrfifo_data),
	.wr_ready			(dac_wrfifo_ready),
	
	//Memory
	.avmm_waitrequest	(mem_waitrequest),
	.avmm_write			(mem_write),
	.avmm_writedata		(mem_writedata),
	.avmm_address		(mem_address),	
	.avmm_burstcount	(mem_burstcount),
	
	//Control signals
	.start					(), // pulse
	.base_address			(),
	.lenght					(),
	);

//-----------  DDR to FX3 (DDR Read) ---------------------------------
rd_master ddrtofx3 #(
	.ADDR_WIDTH(32),
	.AVST_DATA_WIDTH(32),
	.AVMM_DATA_WIDTH(512)
	)
(
	.avst_clk				(clk_fx3),			
	.avmm_clk				(clk_ddr),
	.reset_n				(resetn),
	
	//FX3
	.rd_valid				(adc_rdfifo_valid),
	.rd_data				(adc_rdfifo_data),	//32b
	.rd_ready				(adc_rdfifo_ready),
	
	//Memory
	.avmm_read				(mem_read),		
	.avmm_waitrequest		(mem_waitrequest), 	
	.avmm_readdata			(mem_readdata),	
	.avmm_readdatavalid		(mem_readdatavalid),
	.avmm_address			(mem_address),		
	.avmm_burstcount		(mem_burstcount)
	
	//Control signals
	.start					(adc_start), // pulse
	.base_address			(),
	.lenght					(),
	    );
//--------------------------------------------------------

endmodule

