
module gpif2emu (

	input			clk,  	// clock to fx3
	inout	[31:0] 	data,  	// data bidirectional signal ended
	input	[1:0]	adrs,	// adrs pins 2 bit to selct read write threads
	output			flagA,  // FX3 RX full flag
	output			flagB,	// FX3 RX partial flag delay 10 clocks
	output			flagC, 	// FX3 TX full flag
	output			flagD, 	// FX3 TX partial flag delay 6 clocks
	
	input			slcs_n,	//	Active low chip select.. initiates start of transfer
	input			slwr_n, // Write enable to FX3 RX FIFO
	input			slrd_n, // Read enable to FX3 TX FIFO
	input			sloe_n, // Output enable for the biderectional bus.. 1=> FX3 input 0=> FX3 drives the data bus
	input			pktend_n,		// assert this signal to send small chunks of data. not used now hold it High

);


