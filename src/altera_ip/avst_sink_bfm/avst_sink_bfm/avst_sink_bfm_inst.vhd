	component avst_sink_bfm is
		port (
			clk        : in  std_logic                     := 'X';             -- clk
			reset      : in  std_logic                     := 'X';             -- reset
			sink_data  : in  std_logic_vector(31 downto 0) := (others => 'X'); -- data
			sink_valid : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- valid
			sink_ready : out std_logic                                         -- ready
		);
	end component avst_sink_bfm;

	u0 : component avst_sink_bfm
		port map (
			clk        => CONNECTED_TO_clk,        --       clk.clk
			reset      => CONNECTED_TO_reset,      -- clk_reset.reset
			sink_data  => CONNECTED_TO_sink_data,  --      sink.data
			sink_valid => CONNECTED_TO_sink_valid, --          .valid
			sink_ready => CONNECTED_TO_sink_ready  --          .ready
		);

