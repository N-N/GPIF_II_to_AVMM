
module avst_sink_bfm (
	clk,
	reset,
	sink_data,
	sink_valid,
	sink_ready);	

	input		clk;
	input		reset;
	input	[31:0]	sink_data;
	input	[0:0]	sink_valid;
	output		sink_ready;
endmodule
