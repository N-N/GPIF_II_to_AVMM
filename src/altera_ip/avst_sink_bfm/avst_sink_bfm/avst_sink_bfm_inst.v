	avst_sink_bfm u0 (
		.clk        (<connected-to-clk>),        //       clk.clk
		.reset      (<connected-to-reset>),      // clk_reset.reset
		.sink_data  (<connected-to-sink_data>),  //      sink.data
		.sink_valid (<connected-to-sink_valid>), //          .valid
		.sink_ready (<connected-to-sink_ready>)  //          .ready
	);

