	avst_source_bfm u0 (
		.clk       (<connected-to-clk>),       //       clk.clk
		.reset     (<connected-to-reset>),     // clk_reset.reset
		.src_data  (<connected-to-src_data>),  //       src.data
		.src_valid (<connected-to-src_valid>), //          .valid
		.src_ready (<connected-to-src_ready>)  //          .ready
	);

