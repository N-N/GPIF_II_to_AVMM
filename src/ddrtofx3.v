//////////////////////////////////////////////////////////////////////////////////
// Company: 		Soliton Technologies
// Engineer: 		Prakash/Gokul
// 
// Create Date:   23/08/2014 
// Design Name: 	USB3 FX3
// Module Name:   fx3_main 
// Project Name: 	TSW14J56 _USB3 project
// Target Devices: Arria V
// Tool versions:  Quartus 14
// Description: 
//
// Dependencies: 
//
// Revision: 		0.0
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

//Author: Ebenezer Dwobeng :ebenezer-d@ti.com

module ddrtofx3 #(parameter M=4, S=1, SAMPLES_PER_CLK=4,RD_ADDR_WIDTH = 32, PRBS_L=16, PRBS_M=9,PRBS_N=5
		      )
   (

    input wire 				    clk_fx3,			// clk_fx3 fx3 100MHz? phase?
	 input wire 				    clk_ddr, // clk ddr @256bits
    input wire 				    reset_n,

    // Avalon Streaming Sink Interface
    // Ready Latency = 0
   
    output  				    adc_rdfifo_valid,
    output  [31:0] 		 	adc_rdfifo_data,
    input wire 				 adc_rdfifo_ready,
	 
	 input adc_xfr_enable, // high for the entire duration
	 input adc_start, // pulse

   
	// Avalon Memory Mapped Slave Master: used to write to external DDR3A memory
	output wire						 					mem_avm_m1_read,				// request for reading from ddr
	input wire											mem_avm_m1_waitrequest, 	// ddr bsy
	input wire	[511:0]		mem_avm_m1_readdata,
	output wire						 					mem_avm_m1_write,
	output wire	[511:0]		mem_avm_m1_writedata,
	input wire						 					mem_avm_m1_readdatavalid, 	// read data valid
	output wire	[31:0]			 					mem_avm_m1_address,			//	understand this; prakash
	output wire	[3:0]			 						mem_avm_m1_burstcount		// BURST_LENGTH
	
	);


	//-------------local parameter declarations---------------------------------------------------
	localparam 										BURST_LENGTH = 8;
	localparam 										IDLE = 2'b00;
   localparam 										AVST_READY = 2'b01;

	//------------------------------register declarations-----------------------------------------
	reg 	[1:0] 								state /* synthesis keep */, n_state  /* synthesis keep */;		//state control registers
	reg	[31:0] 								adc_fifo_out_r;
	reg 											mem_rd_valid, n_mem_rd_valid;
	reg 	[31:0] 								adc_fifo_out;
	reg											flush_fifo_dly,n_flush_fifo_dly;
	reg [3:0]									mem_rd_valid_r;
	reg											rd_en_a, n_rd_en_a;					//, rd_en_b, n_rd_en_b;
	reg	[RD_ADDR_WIDTH-1:0]				ddr3a_rd_addr, n_ddr3a_rd_addr;	//,ddr3b_rd_addr, n_ddr3b_rd_addr;
	reg											mem_avm_m1_readdatavalid_dly;		//, mem_avm_m2_readdatavalid_dly;
	reg [511:0]		mem_avm_m1_readdata_dly;			//, mem_avm_m2_readdata_dly;
	reg				aempty_a;//, aempty_b;
	//------------------------------wire declarations-----------------------------------------
	wire 										empty_a /* synthesis keep */;			//, empty_b;
	wire [7:0]									usedw_a /* synthesis keep */;			//, usedw_b;
	
	//-------------------------  FIFO section -------------------------------------------
	wire [31:0]	fifo_out;
	reg empty_a_r;
	
	always @(posedge clk_fx3 or negedge reset_n)
	begin
		if(!reset_n)
		begin
			empty_a_r	<= 1'b0;
			adc_fifo_out <= 32'hDEADDEAD;
		end	
		else
		begin
			empty_a_r	<= empty_a;
			adc_fifo_out <= fifo_out;
		end
	end
	
//---------------------  FIFO - DDR writes FX3 Reads ---------------------------------
		// fifo0 for DDR3A
	ddr2fx3_32bfifo	fifo_inst_0 (
	//write by DDR
	.data 			(mem_avm_m1_readdata_dly), // 256 x 512 bit (word = 512) 
	.wrclk			(clk_ddr),
	.wrreq 			(mem_avm_m1_readdatavalid_dly), // wr enable
	.wrusedw		(usedw_a),						// word counter
	
	//read by FX3 interface
	.rdclk			(clk_fx3),
	.rdreq 			(mem_rd_valid),
	.rdempty		(empty_a),
	.q 				(fifo_out),	//Data to FX3 - LSB FX3 Data
	
	.aclr 			(adc_start)	//changed for flush_fifo_dly to adc_start; Gokul Prasath N
	);
 
//---------------------  FIFO section ends here ---------------------------------
//--------------------------------------------------------------------------------	 	

//--------------------   FX3 sections starts here  control part is here ------------
	always @(posedge clk_fx3 or negedge reset_n)
	begin
		if(!reset_n)
			state 	<= IDLE; //clk_fx3
		else
			state		<= n_state;
	end
	
///  FSM	
	always @ (*)			
	begin
		n_state = state;
		n_mem_rd_valid = mem_rd_valid; 
		n_flush_fifo_dly = 1'b0;

		case(state)
			IDLE:	
			begin	
				n_state = IDLE;
				n_flush_fifo_dly = 1'b0;
				//This change is made to fix the spurs issue occurs when new data is being written to DDR from HSDC Pro 
				if(adc_xfr_enable)//do not read from memory until link has been established
				begin
					n_flush_fifo_dly = 1'b1;
					n_state = AVST_READY;
				end
					
			end
			
			AVST_READY:
			begin
				if (adc_xfr_enable) 
				begin
					if (adc_rdfifo_ready && !(empty_a_r))// | empty_b_r))	//(!(empty_a|empty_b)))// make it almost empty??
						n_mem_rd_valid = 1'b1;	
					else			 
						n_mem_rd_valid = 1'b0;		       
				end				
				else 
					begin
						n_mem_rd_valid  = 1'b0;
						n_state = IDLE;
						n_flush_fifo_dly = 1'b1;
					end
			end
			
			default:
			begin
				n_state = IDLE;
			end
		endcase
	end
//----------------	
	
//---  registering wrt to fx3 clk -------------------------------	
	always @(posedge clk_fx3 or negedge reset_n)
	begin
		if(!reset_n)
		begin
			adc_fifo_out_r   <= 32'h0; //clk_fx3
			mem_rd_valid <= 1'b0; //clk_fx3
			mem_rd_valid_r <= 4'h0; //clk_fx3
			flush_fifo_dly <= 1'b1; //clk_fx3
		end
		else
		begin
			adc_fifo_out_r   <= adc_fifo_out;  // register separately then merge this bus; Prakash 
			mem_rd_valid <= n_mem_rd_valid; 
			mem_rd_valid_r <= {mem_rd_valid_r[2:0],mem_rd_valid};
			flush_fifo_dly <= n_flush_fifo_dly;		
		end  
	end
	
	assign adc_rdfifo_valid = mem_rd_valid_r[2] & !(empty_a_r);		//(!(empty_a_r | empty_b_r)) ;			// informs that the data is valid wrt this signal
	assign adc_rdfifo_data = adc_fifo_out_r; // data from fifo to Fx3 module

//------------  fx3 section ends here ---------------------------------------	

//--------------------   DDR control section ----------------------------------------
// http://www.altera.com/literature/manual/mnl_avalon_spec.pdf  page 23
//--------------------------------------------------------------------------------------
	always @ (*)
	begin
		n_ddr3a_rd_addr = ddr3a_rd_addr;
		n_rd_en_a = 1'b0;
		if (state == IDLE)
		begin
			n_ddr3a_rd_addr = 32'd0;
		end
		if (state == AVST_READY)
		begin
			if(!mem_avm_m1_waitrequest)
			begin
				if (aempty_a)
				begin
					n_rd_en_a = 1'b1;
					n_ddr3a_rd_addr = ddr3a_rd_addr + BURST_LENGTH;				  
				end
			end
		end
	end
//--------
//--------
//----------------- register the signals on dddr clock---------------
	always @(posedge clk_ddr or negedge reset_n)
	begin
		if(!reset_n)
			begin	     		 
				ddr3a_rd_addr 	<= {RD_ADDR_WIDTH{1'b0}}; //clk_ddr
				rd_en_a			<= 1'b0; //clk_ddr
				mem_avm_m1_readdatavalid_dly 	<= 1'b0; //clk_ddr
				mem_avm_m1_readdata_dly 		<= 512'd0; //clk_ddr
				aempty_a			<= 1'b0;
			end
		else
			begin
				ddr3a_rd_addr 		<= n_ddr3a_rd_addr;
				rd_en_a				<= n_rd_en_a;			 
				mem_avm_m1_readdatavalid_dly 	<= mem_avm_m1_readdatavalid; // rd data valid from Avalon bus
				mem_avm_m1_readdata_dly 		<= mem_avm_m1_readdata;			// rd data from avalon bus
				aempty_a <= !usedw_a[7];
			end  
	end

	
	assign mem_avm_m1_address 	= ddr3a_rd_addr;
	assign mem_avm_m1_read 		= n_rd_en_a;
	assign mem_avm_m1_burstcount =   BURST_LENGTH;	
	
	assign mem_avm_m1_write = 1'b0; // irrelevant
//-----------------------------------------------------------------------------------	
//--------------------------  DDR section ends here --------------------------------

	/*assign debug_port = {	2'h0,mem_avm_m1_readdatavalid,
									mem_avm_m2_readdatavalid,mem_avm_m1_waitrequest,
									mem_avm_m2_waitrequest,capture_start,capture,
									mem_avm_m1_read,mem_avm_m2_read,
									adc_rdfifo_valid,adc_rdfifo_ready,
									aempty_a,aempty_b,empty_a,empty_b
								};
	*/
	
	(*preserve*) reg mem_avm_m1_waitrequest_STP;
	(*preserve*) reg [31:0] mem_avm_m1_address_STP;
	
	always @(posedge clk_ddr or negedge reset_n) begin
		mem_avm_m1_waitrequest_STP <= mem_avm_m1_waitrequest; end
		
	always @(posedge clk_ddr or negedge reset_n) begin
		mem_avm_m1_address_STP <= mem_avm_m1_address; end
		
		
endmodule

