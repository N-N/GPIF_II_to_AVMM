onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix unsigned /avmm_master_to_avst_tb/avst_data
add wave -noupdate /avmm_master_to_avst_tb/avst_ready
add wave -noupdate /avmm_master_to_avst_tb/avst_valid
add wave -noupdate /avmm_master_to_avst_tb/avst_clk
add wave -noupdate /avmm_master_to_avst_tb/avmm_master_to_avst_inst/fifo_rdempty
add wave -noupdate /avmm_master_to_avst_tb/avmm_waitrequest
add wave -noupdate /avmm_master_to_avst_tb/avmm_address
add wave -noupdate /avmm_master_to_avst_tb/avmm_burstcount
add wave -noupdate /avmm_master_to_avst_tb/avmm_readdata
add wave -noupdate /avmm_master_to_avst_tb/avmm_clk
add wave -noupdate /avmm_master_to_avst_tb/avst_rstn
add wave -noupdate /avmm_master_to_avst_tb/avmm_rstn
add wave -noupdate /avmm_master_to_avst_tb/start
add wave -noupdate /avmm_master_to_avst_tb/busy
add wave -noupdate /avmm_master_to_avst_tb/burstcount
add wave -noupdate /avmm_master_to_avst_tb/address
add wave -noupdate /avmm_master_to_avst_tb/avmm_read
add wave -noupdate /avmm_master_to_avst_tb/avmm_readdatavalid
add wave -noupdate /avmm_master_to_avst_tb/avmm_master_to_avst_inst/fifo_wr
add wave -noupdate /avmm_master_to_avst_tb/avmm_master_to_avst_inst/fifo_wrusedw
add wave -noupdate /avmm_master_to_avst_tb/avmm_master_to_avst_inst/fifo_aclr
add wave -noupdate /avmm_master_to_avst_tb/avmm_master_to_avst_inst/busy_avmm_clk
add wave -noupdate /avmm_master_to_avst_tb/avmm_master_to_avst_inst/n_avmm_busy
add wave -noupdate /avmm_master_to_avst_tb/avmm_master_to_avst_inst/n_avmm_data_cnt
add wave -noupdate /avmm_master_to_avst_tb/avmm_master_to_avst_inst/n_avmm_address
add wave -noupdate /avmm_master_to_avst_tb/avmm_master_to_avst_inst/n_avst_en
add wave -noupdate /avmm_master_to_avst_tb/avmm_master_to_avst_inst/n_avst_state
add wave -noupdate /avmm_master_to_avst_tb/avmm_master_to_avst_inst/rest_lenght
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2223112 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 404
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {30801920 ps}
