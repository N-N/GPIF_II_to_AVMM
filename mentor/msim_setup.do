# ----------------------------------------
# # TOP-LEVEL TEMPLATE - BEGIN
# #
# # QSYS_SIMDIR is used in the Quartus-generated IP simulation script to
# # construct paths to the files required to simulate the IP in your Quartus
# # project. By default, the IP script assumes that you are launching the
# # simulator from the IP script location. If launching from another
# # location, set QSYS_SIMDIR to the output directory you specified when you
# # generated the IP script, relative to the directory from which you launch
# # the simulator.
# #
set QSYS_SIMDIR ../
# #
# # Source the generated IP simulation script.
source $QSYS_SIMDIR/mentor/msim_setup.tcl
# #
# # Set any compilation options you require (this is unusual).
# set USER_DEFINED_COMPILE_OPTIONS <compilation options>
# #
# # Call command to compile the Quartus EDA simulation library.
dev_com
# #
# # Call command to compile the Quartus-generated IP simulation files.
com
# #
# # Add commands to compile all design files and testbench files, including
# # the top level. (These are all the files required for simulation other
# # than the files compiled by the Quartus-generated IP simulation script)
# #
vlog $QSYS_SIMDIR/src/avst_to_avmm_master.v
vlog $QSYS_SIMDIR/src/avmm_master_to_avst.v
vlog $QSYS_SIMDIR/src/data_sync.v
vlog $QSYS_SIMDIR/src/reset_sync.v
vlog $QSYS_SIMDIR/src/avst_to_avmm_master_tb.sv -L altera_common_sv_packages
vlog $QSYS_SIMDIR/src/avmm_master_to_avst_tb.sv -L altera_common_sv_packages
# #
# # Set the top-level simulation or testbench module/entity name, which is
# # used by the elab command to elaborate the top level.
# #
#set TOP_LEVEL_NAME $QSYS_SIMDIR/src/avst_to_avmm_master_tb.sv
set TOP_LEVEL_NAME $QSYS_SIMDIR/src/avmm_master_to_avst.v
# #
# # Set any elaboration options you require.
# set USER_DEFINED_ELAB_OPTIONS <elaboration options>
# #
# # Call command to elaborate your design and testbench.
elab
# #
# # Run the simulation.
#run -a
# #
# # Report success to the shell.
# exit -code 0
# #
# # TOP-LEVEL TEMPLATE - END
# ----------------------------------------